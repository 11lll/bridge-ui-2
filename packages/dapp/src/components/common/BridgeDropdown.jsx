import {
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
  useBreakpointValue,
} from '@chakra-ui/react';
import { useSettings } from 'contexts/SettingsContext';
import { DownArrowIcon } from 'icons/DownArrowIcon';
import { NetworkIcon } from 'icons/NetworkIcon';
import { networks } from 'lib/networks';
import React, { useCallback, useEffect } from 'react';

export const BridgeDropdown = ({ close }) => {
  const { bridgeDirection, setBridgeDirection } = useSettings();
  const placement = useBreakpointValue({ base: 'bottom', md: 'bottom-end' });

  const setItem = useCallback(
    e => {
      setBridgeDirection(e.target.value, true);
      close();
    },
    [close, setBridgeDirection],
  );

  const networkOptions = Object.keys(networks);
  const isValidNetwork = Object.keys(networks).indexOf(bridgeDirection) >= 0;

  const currentBridgeDirection = isValidNetwork
    ? bridgeDirection
    : networkOptions[0];

  useEffect(() => {
    if (!isValidNetwork) {
      setBridgeDirection(networkOptions[0], true);
    }
  }, [isValidNetwork, networkOptions, setBridgeDirection]);

  return (
    <Menu placement={placement}>
      <MenuButton
        as={Button}
        leftIcon={<NetworkIcon />}
        rightIcon={<DownArrowIcon boxSize="0.5rem" color="white" />}
        color="white"
        bg="none"
        _hover={{ color: '#b92eff', borderBottom: '1px solid #b92eff' }}
        p={2}
      >
        <Text
          color="black"
          textTransform="uppercase"
          fontSize="sm"
          color="white"
        >
          {networks[currentBridgeDirection].label}
        </Text>
      </MenuButton>
      <MenuList border="none" zIndex="3">
        {Object.entries(networks).map(([key, { label }]) => (
          <MenuItem
            value={key}
            onClick={setItem}
            key={key}
            textTransform="uppercase"
            fontWeight="700"
            fontSize="sm"
            justifyContent="center"
            _hover={{ color: '#b92eff' }}
          >
            {label}
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};
