// aviad
import {
  Button,
  Flex,
  Image,
  Input,
  Spinner,
  Text,
  useBreakpointValue,
  useDisclosure,
} from '@chakra-ui/react';
import DropDown from 'assets/drop-down.svg';
import { AdvancedMenu } from 'components/bridge/AdvancedMenu';
import { Balance } from 'components/bridge/cardComponents/Balance';
import { MaxBalance } from 'components/bridge/cardComponents/MaxBalance';
import { ToToken } from 'components/bridge/ToToken';
import { Logo } from 'components/common/Logo';
import { SelectTokenModal } from 'components/modals/SelectTokenModal';
import { useBridgeContext } from 'contexts/BridgeContext';
import { useWeb3Context } from 'contexts/Web3Context';
import { BigNumber, utils } from 'ethers';
import {
  formatValue,
  getNetworkName,
  logError,
  truncateText,
} from 'lib/helpers';
import { fetchTokenBalance } from 'lib/token';
import React, { useCallback, useEffect, useRef, useState } from 'react';

import { SwitchButton } from './SwitchButton';

const useDelay = (fn, ms) => {
  const timer = useRef(0);

  const delayCallBack = useCallback(
    (...args) => {
      clearTimeout(timer.current);
      timer.current = setTimeout(fn.bind(this, ...args), ms || 0);
    },
    [fn, ms],
  );

  return delayCallBack;
};

export const FromToken = () => {
  const { account, providerChainId: chainId } = useWeb3Context();
  const {
    txHash,
    fromToken: token,
    fromBalance: balance,
    setFromBalance: setBalance,
    setAmount,
    amountInput: input,
    setAmountInput: setInput,
  } = useBridgeContext();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const smallScreen = useBreakpointValue({ base: true, lg: false });
  const [balanceLoading, setBalanceLoading] = useState(false);
  const updateAmount = useCallback(() => {
    setAmount(input);
  }, [input, setAmount]);
  const delayedSetAmount = useDelay(updateAmount, 500);

  useEffect(() => {
    let isSubscribed = true;
    if (token && account && chainId === token.chainId) {
      setBalanceLoading(true);
      fetchTokenBalance(token, account)
        .catch(fromBalanceError => {
          logError({ fromBalanceError });
          if (isSubscribed) {
            setBalance(BigNumber.from(0));
            setBalanceLoading(false);
          }
        })
        .then(b => {
          if (isSubscribed) {
            setBalance(b);
            setBalanceLoading(false);
          }
        });
    } else {
      setBalance(BigNumber.from(0));
    }
    return () => {
      isSubscribed = false;
    };
  }, [txHash, token, account, setBalance, setBalanceLoading, chainId]);

  return (
    <Flex
      position="relative"
      borderRadius="0.25rem"
      border={{ base: '1px solid #DAE3F0', lg: 'none' }}
      minH={smallScreen ? '5rem' : 8}
      minW={smallScreen ? '15rem' : undefined}
      background="white"
      padding="1rem"
      borderRadius="17px"
      direction="column"
      width="100%"
    >
      <SelectTokenModal onClose={onClose} isOpen={isOpen} />
      <Flex>
        {/* withdraw Starts AAAAAAAAAAAaAAAAAAAAAAAAAAAAAAAAAA */}
        <Text
          color="black"
          fontSize="m"
          padding="1rem"
          // background="red"
        >
          Withdraw From
        </Text>
      </Flex>
      {token && (
        <Flex
          // padding="1rem"

          direction="column"
          textAlign="right"
          border="1px solid #dddfe0"
          borderRadius="12px"
          height="auto"
          color="#b92eff"
        >
          {/* Top Section starts */}
          <Flex justify="space-between">
            <Text fontWeight="bold" fontSize="m" padding="1rem" color="black">
              {getNetworkName(chainId)}
            </Text>
            <Balance
              balanceLoading={balanceLoading}
              balance={balance}
              token={token}
            />
          </Flex>
          {/* Top Section ends */}
          {/* Buttoms section starts */}
          <Flex justify="space-between" align="center">
            {/* dropdown starts */}
            <Flex
              onClick={onOpen}
              flexGrow="1"
              height="4rem"
              align="center"
              borderTop="1px solid #dddfe0"
              padding="1rem"
            >
              <Flex
                justify="center"
                align="center"
                background="transparent"
                border="1px solid #DAE3F0"
                boxSize={8}
                overflow="hidden"
                borderRadius="50%"
              >
                <Logo uri={token.logoURI} />
              </Flex>
              <Text color="Black" fontSize="lg" fontWeight="bold" mx={2}>
                {truncateText(token.name, 24)}
              </Text>
              <Image src={DropDown} cursor="pointer" />
            </Flex>
            {/* dropdown Ends */}

            {/* input and max starts */}

            <MaxBalance
              input={input}
              setInput={setInput}
              delayedSetAmount={delayedSetAmount}
              balance={balance}
              token={token}
              setAmount={setAmount}
            />

            {/* input and max Ends */}
          </Flex>
          {/* Buttoms section Ends */}
          {/* withdraw Ends AAAAAAAAAAAaAAAAAAAAAAAAAAAAAAAAAA */}
        </Flex>
      )}

      <SwitchButton />
      <Flex direction="column">
        {/* Deposit To Starts AAAAAAAAAAAaAAAAAAAAAAAAAAAAAAAAAA */}
        <Text
          color="black"
          fontSize="m"
          padding="1rem"
          // background="red"
        >
          Deposit To
        </Text>
        <AdvancedMenu />
      </Flex>
      <ToToken />
    </Flex>
  );
};
