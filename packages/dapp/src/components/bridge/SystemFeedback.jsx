import {
  Button,
  Flex,
  Image,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Spinner,
  Text,
} from '@chakra-ui/react';
// import Details from 'assets/details.svg';
import { useBridgeContext } from 'contexts/BridgeContext';
import { formatValue } from 'lib/helpers';
import React, { useCallback, useState } from 'react';

export const SystemFeedback = () => {
  const {
    fromToken: token,
    tokenLimits,
    updateTokenLimits,
  } = useBridgeContext();

  const [loading, setLoading] = useState(false);

  const update = useCallback(async () => {
    setLoading(true);
    await updateTokenLimits();
    setLoading(false);
  }, [updateTokenLimits]);

  return (
    <Popover>
      <PopoverTrigger>
        <Button
          onClick={update}
          _hover={{ bg: '#b92eff', color: 'white' }}
          borderRadius="17px"
        >
          {/* <Image src={Details} mr={2} color="red"/> */}
          <Text>System Feedback</Text>
        </Button>
      </PopoverTrigger>
      <PopoverContent
        border="none"
        minW="20rem"
        w="auto"
        maxW="30rem"
        p="0"
        borderRadius="17px"
      >
        {token && tokenLimits && (
          <PopoverBody
            width="100%"
            align="center"
            fontSize="sm"
            boxShadow="0 0.1rem 1rem #9d00ff"
            background="transparent"
            borderRadius="17px"
          >
            <Flex align="center" justify="space-between">
              <Text color="#9d00ff" textAlign="left">
                Daily Limit
              </Text>
              {loading ? (
                <Spinner size="sm" />
              ) : (
                <Text fontWeight="bold" ml={4} textAlign="right">
                  {`${formatValue(tokenLimits.dailyLimit, token.decimals)} ${
                    token.symbol
                  }`}
                </Text>
              )}
            </Flex>
            <Flex align="center" justify="space-between">
              <Text color="#9d00ff" textAlign="left">
                Max per Tx
              </Text>
              {loading ? (
                <Spinner size="sm" />
              ) : (
                <Text fontWeight="bold" ml={4} textAlign="right">
                  {`${formatValue(tokenLimits.maxPerTx, token.decimals)} ${
                    token.symbol
                  }`}
                </Text>
              )}
            </Flex>
            <Flex align="center" justify="space-between">
              <Text color="#9d00ff" textAlign="left">
                Min per Tx
              </Text>
              {loading ? (
                <Spinner size="sm" />
              ) : (
                <Text fontWeight="bold" ml={4} textAlign="right">
                  {`${formatValue(tokenLimits.minPerTx, token.decimals)} ${
                    token.symbol
                  }`}
                </Text>
              )}
            </Flex>
          </PopoverBody>
        )}
      </PopoverContent>
    </Popover>
  );
};
