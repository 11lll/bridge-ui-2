import {
  Flex,
  Image,
  Link,
  Spinner,
  Text,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';
import UnlockIcon from 'assets/unlock.svg';
import { TxLink } from 'components/common/TxLink';
import { ConfirmTransferModal } from 'components/modals/ConfirmTransferModal';
import { isRebasingToken } from 'components/warnings/RebasingTokenWarning';
import { isSafeMoonToken } from 'components/warnings/SafeMoonTokenWarning';
import { useBridgeContext } from 'contexts/BridgeContext';
import { useWeb3Context } from 'contexts/Web3Context';
import { isRevertedError } from 'lib/amb';
import { handleWalletError } from 'lib/helpers';
import React, { useCallback } from 'react';

export const UnlockAndTransferButton = () => {
  const { providerChainId } = useWeb3Context();

  const { isOpen, onOpen, onClose } = useDisclosure();

  const {
    fromAmount: amount,
    fromBalance: balance,
    fromToken: token,
    toAmountLoading,
    allowed,
    approve,
    unlockLoading,
    approvalTxHash,
    transfer,
  } = useBridgeContext();

  const isTokenRebasing = isRebasingToken(token);
  const isTokenSafeMoon = isSafeMoonToken(token);
  const buttonDisabled =
    allowed || toAmountLoading || isTokenRebasing || isTokenSafeMoon;
  const toast = useToast();
  const showError = useCallback(
    msg => {
      if (msg) {
        toast({
          title: 'Error',
          description: msg,
          status: 'error',
          isClosable: 'true',
        });
      }
    },
    [toast],
  );
  const valid = useCallback(() => {
    if (amount.lte(0)) {
      showError('Please specify amount');
      return false;
    }
    if (balance.lt(amount)) {
      showError('Not enough balance');
      return false;
    }
    return true;
  }, [amount, balance, showError]);

  const approveAndTransfer = () => {
    approve()
      .then(approveData => {
        console.log('approveData', approveData);
        // transfer
        transfer()
          .then(transferData => {
            console.log('transferData', transferData);
          })
          .catch(error => handleWalletError(error, showError));
      })
      .catch(error => {
        if (error && error.message) {
          if (
            isRevertedError(error) ||
            (error.data &&
              (error.data.includes('Bad instruction fe') ||
                error.data.includes('Reverted')))
          ) {
            showError(
              <Text>
                There is problem with the token unlock. Try to revoke previous
                approval if any on{' '}
                <Link
                  href="https://revoke.cash"
                  textDecor="underline"
                  isExternal
                >
                  https://revoke.cash/
                </Link>{' '}
                and try again.
              </Text>,
            );
          } else {
            handleWalletError(error, showError);
          }
        } else {
          showError(
            'Impossible to perform the operation. Reload the application and try again.',
          );
        }
      });
  };

  const onClick = useCallback(() => {
    if (!unlockLoading && !buttonDisabled && valid()) {
      onOpen();
    }
  }, [unlockLoading, buttonDisabled, valid, showError, approve]);

  return (
    <Flex
      as="button"
      color="cyan.500"
      _hover={
        buttonDisabled
          ? undefined
          : {
              color: 'cyan.600',
            }
      }
      cursor={buttonDisabled ? 'not-allowed' : 'pointer'}
      transition="0.25s"
      position="relative"
      opacity={buttonDisabled ? 0.4 : 1}
      onClick={onClick}
      borderRadius="0.25rem"
      w={{ base: '10rem', sm: '12rem', lg: 'auto' }}
      margin="0.1rem"
    >
      <Flex
        w="100%"
        h="100%"
        justify="center"
        align="center"
        bg="black.500"
        p="20px"
        borderRadius="5"
        margin="10px"
        border="1px solid white"
      >
        {unlockLoading ? (
          <TxLink chainId={providerChainId} hash={approvalTxHash}>
            <Spinner color="white" size="sm" />
          </TxLink>
        ) : (
          <>
            <Text color="white" fontWeight="bold">
              {/* {buttonDisabled ? 'Unlocked' : 'Unlock'} */}
              {'UnlockAndTransferButton'}
            </Text>
            <Image src={UnlockIcon} ml={2} />
          </>
        )}
      </Flex>
      <ConfirmTransferModal
        isOpen={isOpen}
        onClose={onClose}
        onClickConfirm={approveAndTransfer}
      />
    </Flex>
  );
};
