// aviad
import { Button, Flex, Input } from '@chakra-ui/react';
import { utils } from 'ethers';
import React from 'react';

export const MaxBalance = props => {
  const { input, setInput, delayedSetAmount, balance, token, setAmount } =
    props;

  return (
    <Flex
      width="50%"
      height="4rem"
      align="center"
      flexGrow="1"
      borderTop="1px solid #dddfe0"
      borderLeft="1px solid #dddfe0"
    >
      {/* 0.0 starts */}
      <Input
        value={input}
        onChange={e => setInput(e.target.value)}
        onKeyUp={delayedSetAmount}
        border="1px solide grey"
        borderRadius="7px"
        padding="17px 20px"
        color="grey"
        fontSize="20px"
        focusBorderColor="none"
      />
      {/* 0.0 Ends */}
      {/* Max starts */}
      <Button
        background="transparent"
        _hover={{ bg: '#b92eff', color: 'white' }}
        border="1px solid #b92eff"
        margin="16px"
        onClick={() => {
          const amountInput = utils.formatUnits(balance, token.decimals);
          setAmount(amountInput);
          setInput(amountInput);
        }}
      >
        MAX
      </Button>
      {/* Max ends */}
    </Flex>
  );
};
