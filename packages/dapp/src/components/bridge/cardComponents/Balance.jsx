import { Flex, Spinner, Text } from '@chakra-ui/react';
import { formatValue } from 'lib/helpers';
import React from 'react';

export const Balance = props => {
  const { balanceLoading, balance, token } = props;
  return balanceLoading ? (
    <Spinner size="sm" color="grey" />
  ) : (
    <Flex
      // background="red"
      padding="16px"
      // grow={1}
      {...{ bottom: '4px', right: 0 }}
    >
      <Text color="black" textAlign="right" fontSize="1rem">
        {`Balance: ${formatValue(balance, token.decimals)}`}
      </Text>
      {/* <Text fontWeight="bold" fontSize="lg" textAlign="right">
                  {getNetworkName(bridgeChainId)}
              </Text> */}
    </Flex>
  );
};
