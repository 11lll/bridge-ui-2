import {
  Box,
  Flex,
  Grid,
  Spacer,
  Text,
  useBreakpointValue,
} from '@chakra-ui/react';
import { AdvancedMenu } from 'components/bridge/AdvancedMenu';
import { FromToken } from 'components/bridge/FromToken';
import { SystemFeedback } from 'components/bridge/SystemFeedback';
import { ToToken } from 'components/bridge/ToToken';
import { TransferButton } from 'components/bridge/TransferButton';
import { UnlockAndTransferButton } from 'components/bridge/UnlockAndTransferButton';
import { UnlockButton } from 'components/bridge/UnlockButton';
import { CoinzillaBannerAd } from 'components/common/CoinzillaBannerAd';
import { CoinzillaTextAd } from 'components/common/CoinzillaTextAd';
import { BridgeLoadingModal } from 'components/modals/BridgeLoadingModal';
import {
  BinancePeggedAssetWarning,
  isERC20ExchangableBinancePeggedAsset,
} from 'components/warnings/BinancePeggedAssetWarning';
import { DaiWarning, isERC20DaiAddress } from 'components/warnings/DaiWarning';
import { GnosisSafeWarning } from 'components/warnings/GnosisSafeWarning';
import {
  InflationaryTokenWarning,
  isInflationaryToken,
} from 'components/warnings/InflationaryTokenWarning';
import {
  isRebasingToken,
  RebasingTokenWarning,
} from 'components/warnings/RebasingTokenWarning';
import { ReverseWarning } from 'components/warnings/ReverseWarning';
import { RPCHealthWarning } from 'components/warnings/RPCHealthWarning';
import {
  isSafeMoonToken,
  SafeMoonTokenWarning,
} from 'components/warnings/SafeMoonTokenWarning';
import { STAKETokenWarning } from 'components/warnings/STAKETokenWarning';
import { useBridgeContext } from 'contexts/BridgeContext';
import { useWeb3Context } from 'contexts/Web3Context';
import { useBridgeDirection } from 'hooks/useBridgeDirection';
import { ADDRESS_ZERO } from 'lib/constants';
import { getNetworkName } from 'lib/helpers';
import { BSC_XDAI_BRIDGE } from 'lib/networks';
import React from 'react';

import { SwitchButton } from './SwitchButton';

export const BridgeTokens = () => {
  const { providerChainId: chainId } = useWeb3Context();
  const {
    getBridgeChainId,
    foreignChainId,
    homeChainId,
    enableReversedBridge,
    bridgeDirection,
  } = useBridgeDirection();
  const { fromToken, toToken } = useBridgeContext();
  const isERC20Dai =
    !!fromToken &&
    fromToken.chainId === foreignChainId &&
    isERC20DaiAddress(fromToken);
  const showReverseBridgeWarning =
    !!toToken &&
    !enableReversedBridge &&
    toToken.chainId === foreignChainId &&
    toToken.address === ADDRESS_ZERO;
  const showBinancePeggedAssetWarning =
    !!fromToken &&
    bridgeDirection === BSC_XDAI_BRIDGE &&
    fromToken.chainId === homeChainId &&
    isERC20ExchangableBinancePeggedAsset(fromToken);
  const isInflationToken = isInflationaryToken(fromToken);
  const isTokenRebasing = isRebasingToken(fromToken);
  const isTokenSafeMoon = isSafeMoonToken(fromToken);

  const smallScreen = useBreakpointValue({ base: true, lg: false });
  const bridgeChainId = getBridgeChainId(chainId);

  return (
    <Flex
      align="center"
      justify="center"
      direction="column"
      w={{ base: undefined, lg: 'calc(100% - 4rem)' }}
      maxW="75rem"
      my="auto"
      mx={{ base: 4, sm: 8 }}
    >
      <CoinzillaTextAd />
      <GnosisSafeWarning noCheckbox />
      <RPCHealthWarning />
      <STAKETokenWarning />
      {isERC20Dai && <DaiWarning />}
      {showReverseBridgeWarning && <ReverseWarning />}
      {showBinancePeggedAssetWarning && (
        <BinancePeggedAssetWarning token={fromToken} />
      )}
      {isInflationToken && (
        <InflationaryTokenWarning token={fromToken} noCheckbox />
      )}
      {isTokenRebasing && <RebasingTokenWarning token={fromToken} />}
      {isTokenSafeMoon && <SafeMoonTokenWarning token={fromToken} />}
      <Flex
        maxW="75rem"
        background="#9100ff47"
        boxShadow="0px 1rem 2rem rgb(14 41 80)"
        borderRadius="1rem"
        direction="column"
        align="center"
        p={{ base: 4, md: 8 }}
      >
        <BridgeLoadingModal />
        <FromToken />
        <UnlockAndTransferButton />
        <SystemFeedback />
      </Flex>
    </Flex>
  );
};
