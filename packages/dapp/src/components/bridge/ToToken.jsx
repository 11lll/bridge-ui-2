import {
  Box,
  Flex,
  Spinner,
  Switch,
  Text,
  useBreakpointValue,
} from '@chakra-ui/react';
import { Balance } from 'components/bridge/cardComponents/Balance';
import { AddToMetamask } from 'components/common/AddToMetamask';
import { Logo } from 'components/common/Logo';
import { useBridgeContext } from 'contexts/BridgeContext';
import { useWeb3Context } from 'contexts/Web3Context';
import { BigNumber, utils } from 'ethers';
import { useBridgeDirection } from 'hooks/useBridgeDirection';
import { fetchToToken } from 'lib/bridge';
import {
  formatValue,
  getNativeCurrency,
  getNetworkName,
  logError,
  truncateText,
} from 'lib/helpers';
import { fetchTokenBalance } from 'lib/token';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

export const ToToken = () => {
  const { account, providerChainId } = useWeb3Context();
  const {
    getBridgeChainId,
    bridgeDirection,
    enableForeignCurrencyBridge,
    foreignChainId,
  } = useBridgeDirection();
  const {
    txHash,
    fromToken,
    toToken: token,
    toAmount: amount,
    toAmountLoading: loading,
    toBalance: balance,
    setToBalance: setBalance,
    shouldReceiveNativeCur,
    setShouldReceiveNativeCur,
    setToToken,
    setLoading,
  } = useBridgeContext();
  const chainId = getBridgeChainId(providerChainId);

  // // Aviads code
  // const bridgeChainId = getBridgeChainId(chainId);

  const smallScreen = useBreakpointValue({ base: true, lg: false });
  const [balanceLoading, setBalanceLoading] = useState(false);

  const nativeCurrency = useMemo(
    () => getNativeCurrency(foreignChainId),
    [foreignChainId],
  );

  const changeToToken = useCallback(async () => {
    setLoading(true);
    setShouldReceiveNativeCur(!shouldReceiveNativeCur);
    setToToken(
      shouldReceiveNativeCur
        ? {
            ...fromToken,
            ...(await fetchToToken(bridgeDirection, fromToken, chainId)),
          }
        : nativeCurrency,
    );
    setLoading(false);
  }, [
    bridgeDirection,
    chainId,
    fromToken,
    nativeCurrency,
    setLoading,
    shouldReceiveNativeCur,
    setShouldReceiveNativeCur,
    setToToken,
  ]);

  useEffect(() => {
    let isSubscribed = true;
    if (token && account && chainId === token.chainId) {
      setBalanceLoading(true);
      fetchTokenBalance(token, account)
        .catch(toBalanceError => {
          logError({ toBalanceError });
          if (isSubscribed) {
            setBalance(BigNumber.from(0));
            setBalanceLoading(false);
          }
        })
        .then(b => {
          if (isSubscribed) {
            setBalance(b);
            setBalanceLoading(false);
          }
        });
    } else {
      setBalance(BigNumber.from(0));
    }
    return () => {
      isSubscribed = false;
    };
  }, [txHash, token, account, setBalance, setBalanceLoading, chainId]);

  return (
    <Flex
    // background="green"
    // padding="1rem"
    >
      {token && (
        <Flex
          direction="column"
          // background="green"
          width="100%"
          justify="right"
          // align="center"
          borderRadius="17px"
          border="1px solid #dddfe0"
        >
          <Flex justify="space-between">
            {/* Balance:0 Starts AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */}
            <Flex
              padding="1rem"
              grow={1}
              // background="blue"
            >
              <Text fontWeight="bold" fontSize="lg" textAlign="right">
                {getNetworkName(chainId)}
              </Text>
            </Flex>

            {/* ---- */}
            <Balance
              balanceLoading={balanceLoading}
              balance={balance}
              token={token}
            />
            {/* ---- */}
            {/* Balance:0 Ends AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */}
          </Flex>
          <Flex align="center" justify="space-between">
            {/* Hex On raspoten Starts AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */}
            <Flex
              borderTop="1px solid #dddfe0"
              padding="1rem"
              height="4rem"
              justifyContent="end"
            >
              <Flex w={8} h={8} overflow="hidden" borderRadius="50%" mr={2}>
                <Logo uri={token.logoURI} reverseFallback />
              </Flex>
              <Text fontSize="lg" fontWeight="bold">
                {truncateText(token.name, 24)}
              </Text>
              <AddToMetamask token={token} ml="0.5rem" asModal />
            </Flex>
            {/* Hex On raspoten Ends AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */}
            {/* balance 0.0 Starts AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */}
            <Flex
              padding="1rem"
              fontSize="18px"
              justifyContent="flex-end"
              grow={1}
              borderLeft="1px solid #dddfe0"
              borderTop="1px solid #dddfe0"
              height="4rem"
            >
              {loading ? (
                <Box mt={{ base: 2, lg: 0 }} mb={{ base: 1, lg: 0 }}>
                  <Spinner color="black" size="sm" />
                </Box>
              ) : (
                <Text>{utils.formatUnits(amount, token.decimals)}</Text>
              )}
              {enableForeignCurrencyBridge &&
                chainId === foreignChainId &&
                fromToken.address.toLowerCase() ===
                  nativeCurrency.homeTokenAddress && (
                  <Flex>
                    <Text>Receive {nativeCurrency.symbol}</Text>
                    <Switch
                      ml={2}
                      colorScheme="blue"
                      isChecked={shouldReceiveNativeCur}
                      onChange={changeToToken}
                    />
                  </Flex>
                )}
            </Flex>
            {/* balance 0.0 Ends AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA */}
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};
