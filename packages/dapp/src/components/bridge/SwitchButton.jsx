import { IconButton, Tooltip } from '@chakra-ui/react';
import { useWeb3Context } from 'contexts/Web3Context';
import { useBridgeDirection } from 'hooks/useBridgeDirection';
import { useSwitchChain } from 'hooks/useSwitchChain';
import { SwitchIcon } from 'icons/SwitchIcon';
import React, { useCallback, useMemo } from 'react';

export const SwitchButton = () => {
  const { providerChainId, isMetamask } = useWeb3Context();
  const { getBridgeChainId } = useBridgeDirection();
  const bridgeChainId = useMemo(
    () => getBridgeChainId(providerChainId),
    [providerChainId, getBridgeChainId],
  );
  const switchChain = useSwitchChain();
  const switchOnClick = useCallback(
    () => switchChain(bridgeChainId),
    [switchChain, bridgeChainId],
  );

  const isDefaultChain = [1, 3, 4, 5, 42].includes(bridgeChainId);
  const isMobileBrowser = navigator?.userAgent?.includes('Mobile') || false;
  const buttonWillWork =
    isMetamask && (isMobileBrowser ? !isDefaultChain : true);

  return buttonWillWork ? (
    <Tooltip label="Switch direction of bridge" closeOnClick={false}>
      <IconButton
        marginTop="1rem"
        // background="red"
        transform="rotate(90deg)"
        width="10px"
        margin="1rem auto 0 auto"
        height="60px"
        background="trasparant"
        icon={<SwitchIcon boxSize="2rem" />}
        _hover={{ bg: 'blackAlpha.100' }}
        onClick={switchOnClick}
      />
    </Tooltip>
  ) : null;
};
